/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.facet.FacetManager;
import com.intellij.facet.FacetType;
import com.intellij.framework.FrameworkTypeEx;
import com.intellij.framework.addSupport.FrameworkSupportInModuleConfigurable;
import com.intellij.framework.addSupport.FrameworkSupportInModuleProvider;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.ide.util.frameworkSupport.FrameworkSupportModel;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.Result;
import com.intellij.openapi.application.WriteAction;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.JavaModuleType;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleType;
import com.intellij.openapi.roots.ContentEntry;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.roots.OrderRootType;
import com.intellij.openapi.roots.libraries.Library;
import com.intellij.openapi.roots.libraries.LibraryTable;
import com.intellij.openapi.roots.libraries.LibraryTablesRegistrar;
import com.intellij.openapi.roots.ui.configuration.FacetsProvider;
import com.intellij.openapi.startup.StartupManager;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.jps.model.java.JavaResourceRootType;
import org.siani.javafmi.plugin.model.PluginConstants;
import org.siani.javafmi.plugin.actions.CreateFmuFileAction;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

import static java.io.File.separator;

class JavaFMISupportProvider extends FrameworkSupportInModuleProvider {

	private static final Logger LOG = Logger.getInstance(JavaFMISupportProvider.class.getName());
	private static final String RES = "res";
	private static final String FMU_FRAMEWORK = "fmu-framework";


	@NotNull
	@Override
	public FrameworkTypeEx getFrameworkType() {
		return JavaFMIFrameworkType.getFrameworkType();
	}

	@Override
	public boolean isEnabledForModuleType(@NotNull ModuleType moduleType) {
		return moduleType instanceof JavaModuleType;
	}

	@Override
	public boolean isSupportAlreadyAdded(@NotNull Module module, @NotNull FacetsProvider facetsProvider) {
		return !facetsProvider.getFacetsByType(module, JavaFMIFacet.ID).isEmpty();
	}

	@NotNull
	@Override
	public FrameworkSupportInModuleConfigurable createConfigurable(@NotNull FrameworkSupportModel model) {
		return new FMISupportConfigurable(this, model);
	}

	void addSupport(final Module module, final ModifiableRootModel rootModel, String[] platforms) {
		createResources(rootModel.getContentEntries()[0]);
		addFmuFrameworkLibrary(rootModel);
		final JavaFMIFacet facet = createFacetConfiguration(module);
		fillFacetConfiguration(facet, platforms);
		if (!module.getProject().isInitialized())
			StartupManager.getInstance(module.getProject()).registerPostStartupActivity(() -> createFmuClass(module, rootModel));
		else createFmuClass(module, rootModel);
	}

	private void createFmuClass(Module module, ModifiableRootModel rootModel) {
		ApplicationManager.getApplication().runWriteAction(() -> createFmuClass(module, rootModel, StringFormatter.toCamelCase(module.getName(), "-")));
	}

	private void addFmuFrameworkLibrary(ModifiableRootModel rootModel) {
		ApplicationManager.getApplication().runWriteAction(() -> {
			final LibraryTable libraryTable = LibraryTablesRegistrar.getInstance().getLibraryTable(rootModel.getProject());
			for (Library library : libraryTable.getLibraries())
				if (FMU_FRAMEWORK.equals(library.getName())) {
					rootModel.addLibraryEntry(library);
					return;
				}
			addFrameworkLibToProject(rootModel);
		});
	}

	private void addFrameworkLibToProject(ModifiableRootModel rootModel) {
		final Library library = addProjectLibrary(rootModel.getModule(), FMU_FRAMEWORK, libraryPath().getParentFile(), new VirtualFile[]{VfsUtil.findFileByIoFile(libraryPath(), true)});
		rootModel.addLibraryEntry(library);
	}

	private static Library addProjectLibrary(final Module module, final String name, final File jarsDirectory, final VirtualFile[] sources) {
		return new WriteAction<Library>() {
			protected void run(@NotNull final Result<Library> result) throws MalformedURLException {
				final LibraryTable libraryTable = LibraryTablesRegistrar.getInstance().getLibraryTable(module.getProject());
				Library library = libraryTable.getLibraryByName(name);
				if (library == null) {
					library = libraryTable.createLibrary(name);
					final Library.ModifiableModel model = library.getModifiableModel();
					final VirtualFile vFile = VfsUtil.findFileByURL(jarsDirectory.toURI().toURL());
					if (vFile == null) return;
					vFile.refresh(true, true);
					model.addJarDirectory(vFile, false, OrderRootType.CLASSES);
					for (VirtualFile sourceRoot : sources) model.addRoot(sourceRoot, OrderRootType.SOURCES);
					model.commit();
				}
				result.setResult(library);
			}
		}.execute().getResultObject();
	}


	@NotNull
	private File libraryPath() {
		return new File(com.intellij.openapi.application.PathManager.getPluginsPath(), PluginConstants.PLUGIN_NAME + separator + "lib" + separator + PluginConstants.FRAMEWORK);
	}

	private JavaFMIFacet createFacetConfiguration(Module module) {
		FacetType<JavaFMIFacet, JavaFMIFacetConfiguration> facetType = JavaFMIFacet.getFacetType();
		return FacetManager.getInstance(module).addFacet(facetType, facetType.getDefaultFacetName(), null);
	}

	private void fillFacetConfiguration(JavaFMIFacet taraFacet, String[] platforms) {
		final JavaFMIFacetConfiguration conf = taraFacet.getConfiguration();
		conf.platforms(platforms);
	}

	private void createResources(ContentEntry contentEntry) {
		try {
			VirtualFile file = contentEntry.getFile();
			if (file == null) return;
			VirtualFile sourceRoot;
			if ((sourceRoot = file.findChild(RES)) == null) sourceRoot = file.createChildDirectory(null, RES);
			contentEntry.addSourceFolder(sourceRoot, JavaResourceRootType.RESOURCE);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private void createFmuClass(Module module, ModifiableRootModel rootModel, String name) {
		final VirtualFile srcRoot = getSrcRoot(getSourceRoots(rootModel));
		final PsiDirectory directory = PsiManager.getInstance(module.getProject()).findDirectory(srcRoot);
		if (directory == null) return;
		final PsiDirectory subdirectory = directory.createSubdirectory("siani").createSubdirectory("javafmi");
		new WriteCommandAction(module.getProject()) {
			@Override
			protected void run(@NotNull Result result) throws Throwable {
				final PsiFile fmuFile = CreateFmuFileAction.createFromTemplate(subdirectory, name, name + "." + JavaFileType.DEFAULT_EXTENSION, "FMUFile." + JavaFileType.INSTANCE.getDefaultExtension(), true, "NAME", name);
				fmuFile.navigate(true);

			}
		}.execute();
	}

	private static List<VirtualFile> getSourceRoots(ModifiableRootModel rootModel) {
		final Set<VirtualFile> result = new LinkedHashSet<>();
		result.addAll(Arrays.asList(rootModel.getSourceRoots()));
		return new ArrayList<>(result);
	}

	private static VirtualFile getSrcRoot(Collection<VirtualFile> virtualFiles) {
		for (VirtualFile file : virtualFiles)
			if (file.isDirectory() && "src".equals(file.getName())) return file;
		return null;
	}
}
