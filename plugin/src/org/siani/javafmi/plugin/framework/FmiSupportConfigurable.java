/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.framework.addSupport.FrameworkSupportInModuleConfigurable;
import com.intellij.ide.util.frameworkSupport.FrameworkSupportModel;
import com.intellij.ide.util.frameworkSupport.FrameworkSupportModelListener;
import com.intellij.ide.util.frameworkSupport.FrameworkSupportProvider;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModifiableModelsProvider;
import com.intellij.openapi.roots.ModifiableRootModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

class FMISupportConfigurable extends FrameworkSupportInModuleConfigurable implements FrameworkSupportModelListener {

	private JavaFMISupportProvider provider;
	private final Project project;
	private JPanel errorPanel;
	private JPanel myMainPanel;
	private JRadioButton cosimulationRadioButton;
	private JRadioButton modelExchangeRadioButton;
	private JCheckBox win64;
	private JCheckBox lin64;
	private JCheckBox win32;
	private JCheckBox lin32;
	private JCheckBox macOs;
	private JCheckBox java8CheckBox;


	FMISupportConfigurable(JavaFMISupportProvider provider, FrameworkSupportModel model) {
		this.provider = provider;
		this.project = model.getProject();
		model.addFrameworkListener(this);
	}

	@Nullable
	@Override
	public JComponent createComponent() {
		return myMainPanel;
	}


	@Override
	public void addSupport(@NotNull Module module,
						   @NotNull ModifiableRootModel rootModel,
						   @NotNull ModifiableModelsProvider modifiableModelsProvider) {
		provider.addSupport(module, rootModel, platforms());
	}

	@Override
	public void onFrameworkSelectionChanged(boolean selected) {
		cosimulationRadioButton.setEnabled(false);
		modelExchangeRadioButton.setEnabled(false);
		java8CheckBox.setEnabled(false);
	}

	@Override
	public void frameworkSelected(@NotNull FrameworkSupportProvider provider) {
	}

	@Override
	public void frameworkUnselected(@NotNull FrameworkSupportProvider provider) {
	}

	@Override
	public void wizardStepUpdated() {
	}

	String[] platforms() {
		List<String> platforms = new ArrayList<>();
		if (win64.isSelected()) platforms.add(win64.getText());
		if (lin64.isSelected()) platforms.add(lin64.getText());
		return platforms.toArray(new String[platforms.size()]);
	}

}
