/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.framework;

import com.intellij.ide.util.importProject.ProjectDescriptor;
import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.importSources.DetectedContentRoot;
import com.intellij.ide.util.projectWizard.importSources.DetectedProjectRoot;
import com.intellij.ide.util.projectWizard.importSources.ProjectFromSourcesBuilder;
import com.intellij.ide.util.projectWizard.importSources.ProjectStructureDetector;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.JavaModuleType;
import com.intellij.openapi.util.io.FileUtilRt;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class JavaFMIProjectStructureDetector extends ProjectStructureDetector {

	private static final Logger LOG = Logger.getInstance("#FmiProjectStructureDetector");


	@NotNull
	@Override
	public DirectoryProcessingResult detectRoots(@NotNull File dir,
	                                             @NotNull File[] children,
	                                             @NotNull File base,
	                                             @NotNull List<DetectedProjectRoot> result) {
		LOG.info("Detecting roots under " + dir);
		for (File child : children) {
			final String name = child.getName();
			if (isFmu(name)) {
				LOG.info("Found FMU file " + child.getPath());
				result.add(new DetectedContentRoot(dir, "FMU", JavaModuleType.getModuleType(), JavaModuleType.getModuleType()));
				return DirectoryProcessingResult.SKIP_CHILDREN;
			}
		}
		return DirectoryProcessingResult.PROCESS_CHILDREN;
	}

	private boolean isFmu(String name) {
		return FileUtilRt.extensionEquals(name, "java");
	}

	@Override
	public void setupProjectStructure(@NotNull Collection<DetectedProjectRoot> roots,
	                                  @NotNull ProjectDescriptor projectDescriptor,
	                                  @NotNull ProjectFromSourcesBuilder builder) {
		builder.setupModulesByContentRoots(projectDescriptor, roots);
	}

	@Override
	public List<ModuleWizardStep> createWizardSteps(ProjectFromSourcesBuilder builder, ProjectDescriptor projectDescriptor, javax.swing.Icon stepIcon) {
		return Collections.emptyList();
	}
}
