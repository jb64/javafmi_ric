/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Unit.java
 *
 * Created on 3. September 2001, 18:40
 */

package org.siani.javafmi.plugin.inspector.chart.aw.util.units;


/**
 *  @see aw.util.units.UnitFactory
 *  @see aw.util.units.UnitSystem
 *  @see aw.util.units.UnitSystemSI
 *
 * @author  <a href='mailto:Achim.Westermann@gmx.de'>Achim Westermann</a>
 * @version 1.0
 */
public abstract class Unit extends Object {

    public double factor;
    protected String prefix;
    protected int decimals = 2;
    
    /**
     *  Transforms the given absolute value into the represented unit value 
     *  by dividing by the specific factor;
     *  The result is rounded using the actual decimal setting.
     **/
    public double getValue(double value){
       return round(value/this.factor);
    }
    public String getLabel(double value){
        return new StringBuffer().append(round(value/this.factor)).append(" ").append(this.prefix).toString();
    }
    
    public String getLabel(){
        return new StringBuffer().append(this.prefix).toString();
    }
    
    public String toString(){
        return this.getLabel();
    }
    /**
     *  factor is public!
     **/
    public double getFactor(){
        return this.factor;
    }
    
    public String getPrefix(){
        return this.prefix;
    }
    
    
    public void setDecimals(int aftercomma){
        if(aftercomma>=0)
            this.decimals = aftercomma;
    }
    public int getDecimals(){
        return this.decimals;
    }
    
    public double round(double value){
        double tmp = Math.pow(10,this.decimals);
        return (Math.floor(value*tmp + 0.5d))/tmp;
    }
}
