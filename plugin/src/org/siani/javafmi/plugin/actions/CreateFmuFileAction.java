/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.siani.javafmi.plugin.actions;

import com.intellij.codeInsight.actions.ReformatCodeProcessor;
import com.intellij.ide.actions.CreateFileFromTemplateDialog;
import com.intellij.ide.actions.JavaCreateTemplateInPackageAction;
import com.intellij.ide.fileTemplates.FileTemplate;
import com.intellij.ide.fileTemplates.FileTemplateManager;
import com.intellij.ide.fileTemplates.JavaTemplateUtil;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.siani.javafmi.plugin.framework.JavaFMIFacet;
import org.siani.javafmi.plugin.framework.StringFormatter;
import org.siani.javafmi.plugin.framework.ModuleProvider;

import java.util.Map;
import java.util.Properties;

import static org.siani.javafmi.plugin.model.JavaFMIIcons.ICON_16;


public class CreateFmuFileAction extends JavaCreateTemplateInPackageAction<PsiJavaFile> {
	static final String NAME_TEMPLATE_PROPERTY = "NAME";
	static final String LOW_CASE_NAME_TEMPLATE_PROPERTY = "lowCaseName";

	public CreateFmuFileAction() {
		super("New FMU file", "Creates a FMU implementation", ICON_16, true);
	}

	@Override
	protected void buildDialog(Project project, PsiDirectory directory, CreateFileFromTemplateDialog.Builder builder) {
		builder.setTitle("Enter cosimulation for new FMU File");
		String fmu = "FMUFile." + JavaFileType.INSTANCE.getDefaultExtension();
		builder.addKind("File", ICON_16, fmu);
	}

	@Override
	protected String getActionName(PsiDirectory directory, String newName, String templateName) {
		return "Creates FMU file";
	}

	@Nullable
	@Override
	protected PsiElement getNavigationElement(@NotNull PsiJavaFile createdElement) {
		return createdElement;
	}

	@Override
	protected boolean isAvailable(DataContext dataContext) {
		PsiElement data = CommonDataKeys.PSI_ELEMENT.getData(dataContext);
		if (!(data instanceof PsiFile || data instanceof PsiDirectory)) return false;
		Module module = ModuleProvider.getModuleOf(data);
		return super.isAvailable(dataContext) && JavaFMIFacet.isOfType(module);
	}

	@Nullable
	@Override
	public PsiJavaFile doCreate(PsiDirectory directory, String newName, String templateName) throws IncorrectOperationException {
		String fileName = newName + "." + JavaFileType.INSTANCE.getDefaultExtension();
		Module module = ModuleProvider.getModuleOf(directory);
		JavaFMIFacet facet = JavaFMIFacet.of(module);
		if (facet == null) throw new IncorrectOperationException();
		PsiFile file = createFromTemplate(directory, newName, fileName, templateName, true, "NAME", StringFormatter.toCamelCase(module.getName(), "-"));
		return file instanceof PsiJavaFile ? (PsiJavaFile) file : error();
	}

	private PsiJavaFile error() {
		throw new IncorrectOperationException("error");
	}

	public static PsiFile createFromTemplate(@NotNull final PsiDirectory directory,
											 @NotNull final String name,
											 @NotNull String fileName,
											 @NotNull String templateName,
											 boolean allowReformatting,
											 @NonNls String... parameters) throws IncorrectOperationException {
		final FileTemplate template = FileTemplateManager.getDefaultInstance().getJ2eeTemplate(templateName);
		Project project = directory.getProject();
		Properties properties = new Properties(FileTemplateManager.getDefaultInstance().getDefaultProperties());
		properties.setProperty("PROJECT_NAME", project.getName());
		JavaTemplateUtil.setPackageNameAttribute(properties, directory);
		properties.setProperty(NAME_TEMPLATE_PROPERTY, name);
		properties.setProperty(LOW_CASE_NAME_TEMPLATE_PROPERTY, name.substring(0, 1).toLowerCase() + name.substring(1));
		for (int i = 0; i < parameters.length; i += 2) properties.setProperty(parameters[i], parameters[i + 1]);
		String text;
		try {
			text = template.getText(properties);
		} catch (Exception e) {
			throw new RuntimeException("Unable to load template for " + FileTemplateManager.getDefaultInstance().internalTemplateToSubject(templateName), e);
		}
		final PsiFileFactory factory = PsiFileFactory.getInstance(project);
		PsiFile file = factory.createFileFromText(fileName, JavaFileType.INSTANCE, text);
		file = (PsiFile) directory.add(file);
		if (file != null && allowReformatting && template.isReformatCode())
			new ReformatCodeProcessor(project, file, null, false).run();
		return file;
	}


	@Override
	protected void postProcess(PsiJavaFile createdElement, String templateName, Map<String, String> customProperties) {
		super.postProcess(createdElement, templateName, customProperties);
		createdElement.navigate(true);
	}
}