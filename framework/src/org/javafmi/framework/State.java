/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.framework;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class State {

	private final double time;
	Map<String, Object> attributes = new HashMap<>();

	public State(double time) {
		this.time = time;
	}

	public Object get(String key) {
		return attributes.get(key);
	}

	public Object put(String key, Object value) {
		return attributes.put(key, value);
	}

	public Set<String> keySet() {
		return attributes.keySet();
	}

	public double time() {
		return time;
	}
}
