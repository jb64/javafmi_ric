/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.framework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.time.Instant.now;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class FmiContainer {

    private String generationTool = "";
    private String generationDateAndTime = ISO_INSTANT.format(now().truncatedTo(SECONDS));

    private static String value(Enum value) {
        return value != null ? value.name() : null;
    }

    public void generationTool(String generationTool) {
        this.generationTool = generationTool;
    }

    public Model model(String name) {
        return new Model(name);
    }

    public Var variable(String name) {
        return new Var(name);
    }

    public ModelStructure modelStructure() {
        return new ModelStructure();
    }

    public Unknown unknown(String index) {
        return new Unknown(index);
    }

    @SuppressWarnings("unused")
    public enum Causality {
        parameter, calculatedParameter, input, output, local, independent
    }

    @SuppressWarnings("unused")
    public enum Variability {
        constant, fixed, tunable, discrete, continuous
    }

    @SuppressWarnings("unused")
    public enum Initial {
        exact, aprox, calculated
    }

    public class Model {
        protected Double startTime;
        protected Double stopTime;
        protected Double tolerance;
        protected Double stepSize;
        private String fmiVersion = "2.0";
        private String modelName;
        private String guid = String.valueOf(System.currentTimeMillis());
        private String description;
        private String author;
        private String version;
        private String copyright;
        private String license;
        private String variableNamingConvention;
        private Integer numberOfEventIndicators;
        private Boolean canHandleVariableStepSize = true;
        private Boolean canInterpolateInputs;
        private Integer maxOutputDerivativeOrder;
        private Boolean canRunAsynchronously;
        private Boolean canBeInstantiatedOnlyOncePerProcess;
        private Boolean canNotUseMemoryManagementFunctions;
        private Boolean canGetAndSetFMUState;
        private Boolean canSerializeFMUstate;
        private Boolean providesDirectionalDerivative;
        private List<ModelDescription.ScalarVariable> variables = new ArrayList<>();
        private ModelStructure modelModelStructure = new ModelStructure();

        public Model(String modelName) {
            this.modelName = modelName;
        }

        public Model guid(String guID) {
            this.guid = guID;
            return this;
        }

        public Model description(String description) {
            this.description = description;
            return this;
        }

        public Model author(String author) {
            this.author = author;
            return this;
        }

        public Model modelVersion(String modelVersion) {
            this.version = modelVersion;
            return this;
        }

        public Model copyright(String copyright) {
            this.copyright = copyright;
            return this;
        }

        public Model license(String license) {
            this.license = license;
            return this;
        }

        public Model variableNamingConvention(ModelDescription.VariableNamingConvention variableNamingConvention) {
            this.variableNamingConvention = variableNamingConvention.toString();
            return this;
        }

        public Model numberOfEventIndicators(int numberOfEventIndicators) {
            this.numberOfEventIndicators = numberOfEventIndicators;
            return this;
        }

        public Model canHandleVariableCommunicationStepSize(boolean canHandleVariableStepSize) {
            this.canHandleVariableStepSize = canHandleVariableStepSize;
            return this;
        }

        public Model canInterpolateInputs(boolean canInterpolateInputs) {
            this.canInterpolateInputs = canInterpolateInputs;
            return this;
        }

        public Model maxOutputDerivativeOrder(int maxOutputDerivativeOrder) {
            this.maxOutputDerivativeOrder = maxOutputDerivativeOrder;
            return this;
        }

        public Model canRunAsynchronuously(boolean canRunAsynchronuously) {
            this.canRunAsynchronously = canRunAsynchronuously;
            return this;
        }

        public Model canBeInstantiatedOnlyOncePerProcess(boolean canBeInstantiatedOnlyOncePerProcess) {
            this.canBeInstantiatedOnlyOncePerProcess = canBeInstantiatedOnlyOncePerProcess;
            return this;
        }

        public Model canNotUseMemoryManagementFunctions(boolean canNotUseMemoryManagementFunctions) {
            this.canNotUseMemoryManagementFunctions = canNotUseMemoryManagementFunctions;
            return this;
        }

        public Model canGetAndSetFMUstate(boolean canGetAndSetFMUstate) {
            this.canGetAndSetFMUState = canGetAndSetFMUstate;
            return this;
        }

        public Model canSerializeFMUstate(boolean canSerializeFMUstate) {
            this.canSerializeFMUstate = canSerializeFMUstate;
            return this;
        }

        public Model providesDirectionalDerivative(boolean providesDirectionalDerivative) {
            this.providesDirectionalDerivative = providesDirectionalDerivative;
            return this;
        }

        public Model startTime(double startTime) {
            this.startTime = startTime;
            return this;
        }

        public Model stopTime(double stopTime) {
            this.stopTime = stopTime;
            return this;
        }

        public Model tolerance(double tolerance) {
            this.tolerance = tolerance;
            return this;
        }

        public Model stepSize(double stepSize) {
            this.stepSize = stepSize;
            return this;
        }

        public Model add(ScalarVariable variable) {
            variable.index = variables.size() + 1;
            variables.add(variable.terminate());
            return this;
        }

        public Model add(ModelStructure modelModelStructure) {
            this.modelModelStructure = modelModelStructure;
            return this;
        }

        public ModelDescription terminate() {
            return new ModelDescription() {
                @Override
                public String fmiVersion() {
                    return fmiVersion;
                }

                @Override
                public String modelName() {
                    return modelName;
                }

                @Override
                public String author() {
                    return author;
                }

                @Override
                public String guid() {
                    return guid;
                }

                @Override
                public String description() {
                    return description;
                }

                @Override
                public String version() {
                    return version;
                }

                @Override
                public String copyright() {
                    return copyright;
                }

                @Override
                public String license() {
                    return license;
                }

                @Override
                public String generationTool() {
                    return generationTool;
                }

                @Override
                public String generationDateAndTime() {
                    return generationDateAndTime;
                }

                @Override
                public String variableNamingConvention() {
                    return variableNamingConvention;
                }

                @Override
                public Integer numberOfEventIndicators() {
                    return numberOfEventIndicators;
                }

                @Override
                public CoSimulation coSimulation() {
                    return new CoSimulation() {
                        @Override
                        public String modelIdentifier() {
                            return modelName;
                        }

                        @Override
                        public Boolean needsExecutionTool() {
                            return true;
                        }

                        @Override
                        public Boolean canHandleVariableStepSize() {
                            return canHandleVariableStepSize;
                        }

                        @Override
                        public Boolean canInterpolateInputs() {
                            return canInterpolateInputs;
                        }

                        @Override
                        public Integer maxOutputDerivativeOrder() {
                            return maxOutputDerivativeOrder;
                        }

                        @Override
                        public Boolean canRunAsynchronously() {
                            return canRunAsynchronously;
                        }

                        @Override
                        public Boolean canBeInstantiatedOnlyOncePerProcess() {
                            return canBeInstantiatedOnlyOncePerProcess;
                        }

                        @Override
                        public Boolean canNotUseMemoryManagementFunctions() {
                            return canNotUseMemoryManagementFunctions;
                        }

                        @Override
                        public Boolean canGetAndSetFMUstate() {
                            return canGetAndSetFMUState;
                        }

                        @Override
                        public Boolean canSerializeFMUstate() {
                            return canSerializeFMUstate;
                        }

                        @Override
                        public Boolean providesDirectionalDerivative() {
                            return providesDirectionalDerivative;
                        }
                    };
                }

                @Override
                public DefaultExperiment defaultExperiment() {
                    return new DefaultExperiment() {
                        @Override
                        public Double startTime() {
                            return startTime;
                        }

                        @Override
                        public Double stopTime() {
                            return stopTime;
                        }

                        @Override
                        public Double tolerance() {
                            return tolerance;
                        }

                        @Override
                        public Double stepSize() {
                            return stepSize;
                        }
                    };
                }

                @Override
                public List<ScalarVariable> variables() {
                    return Collections.unmodifiableList(variables);
                }


                @Override
                public ModelStructure modelStructure() {
                    return modelModelStructure.terminate();
                }

            };
        }

    }

    public class Var {

        private String name;

        public Var(String name) {
            this.name = name;
        }

        public RealVariable asReal() {
            return new RealVariable(name, 0);
        }

        public IntegerVariable asInteger() {
            return new IntegerVariable(name, 0);
        }

        public BooleanVariable asBoolean() {
            return new BooleanVariable(name, 0);
        }

        public StringVariable asString() {
            return new StringVariable(name, 0);
        }

    }

    @SuppressWarnings("unchecked")
    protected abstract class ScalarVariable<T extends ScalarVariable> {
        protected String name;
        protected int index;

        protected String description;
        protected Causality causality;
        protected Variability variability;
        protected Initial initial;
        protected boolean canHandleMultipleSetPerTime = true;

        public ScalarVariable(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public T description(String description) {
            this.description = description;
            return (T) this;
        }

        public T causality(Causality causality) {
            this.causality = causality;
            return (T) this;
        }

        public T variability(Variability variability) {
            this.variability = variability;
            return (T) this;
        }

        public T initial(Initial initial) {
            this.initial = initial;
            return (T) this;
        }

        public T canHandleMultipleSetPerTime(boolean canHandleMultipleSetPerTime) {
            this.canHandleMultipleSetPerTime = canHandleMultipleSetPerTime;
            return (T) this;
        }

        public abstract ModelDescription.ScalarVariable terminate();

    }

    public class RealVariable extends ScalarVariable<RealVariable> {
        private String quantity;
        private String unit;
        private String displayUnit;
        private Boolean relativeQuantity;
        private Double min;
        private Double max;
        private Double nominal;
        private Boolean isUnbounded;
        private Double start;
        private Integer derivativeIndex;
        private Boolean canReinit;

        public RealVariable(String name, int valueReference) {
            super(name, valueReference);
        }

        public RealVariable quantity(String quantity) {
            this.quantity = quantity;
            return this;
        }

        public RealVariable unit(String unit) {
            this.unit = unit;
            return this;
        }

        public RealVariable displayUnit(String displayUnit) {
            this.displayUnit = displayUnit;
            return this;
        }

        public RealVariable relativeQuantity(boolean relativeQuantity) {
            this.relativeQuantity = relativeQuantity;
            return this;
        }

        public RealVariable min(double min) {
            this.min = min;
            return this;
        }

        public RealVariable max(double max) {
            this.max = max;
            return this;
        }

        public RealVariable nominal(double nominal) {
            this.nominal = nominal;
            return this;
        }

        public RealVariable isUnbounded(boolean unbounded) {
            this.isUnbounded = unbounded;
            return this;
        }

        public RealVariable start(double start) {
            this.start = start;
            return this;
        }

        public RealVariable derivative(int derivativeIndex) {
            this.derivativeIndex = derivativeIndex;
            return this;
        }

        public RealVariable reinit(boolean reinit) {
            this.canReinit = reinit;
            return this;
        }

        @Override
        public ModelDescription.RealVariable terminate() {
            return new ModelDescription.RealVariable() {
                @Override
                public String name() {
                    return name;
                }

                @Override
                public int valueReference() {
                    return index;
                }

                @Override
                public String description() {
                    return description;
                }

                @Override
                public String causality() {
                    return value(causality);
                }

                @Override
                public String variability() {
                    return value(variability);
                }

                @Override
                public String initial() {
                    return value(initial);
                }

                @Override
                public Integer previous() {
                    return index > 1 ? index - 1 : null;
                }

                @Override
                public boolean canHandleMultipleSetPerTimeInstant() {
                    return canHandleMultipleSetPerTime;
                }

                @Override
                public String quantity() {
                    return quantity;
                }

                @Override
                public String unit() {
                    return unit;
                }

                @Override
                public String displayUnit() {
                    return displayUnit;
                }

                @Override
                public Boolean relativeQuantity() {
                    return relativeQuantity;
                }

                @Override
                public Double min() {
                    return min;
                }

                @Override
                public Double max() {
                    return max;
                }

                @Override
                public Double nominal() {
                    return nominal;
                }

                @Override
                public Boolean unbounded() {
                    return isUnbounded;
                }

                @Override
                public Double start() {
                    return start;
                }

                @Override
                public Integer derivative() {
                    return derivativeIndex;
                }

                @Override
                public Boolean reinit() {
                    return canReinit;
                }

            };
        }

    }

    public class IntegerVariable extends ScalarVariable<IntegerVariable> {
        private String quantity;
        private Integer min;
        private Integer max;
        private Integer start;

        public IntegerVariable(String name, int valueReference) {
            super(name, valueReference);
        }

        public IntegerVariable quantity(String quantity) {
            this.quantity = quantity;
            return this;
        }

        public IntegerVariable min(int min) {
            this.min = min;
            return this;
        }

        public IntegerVariable max(int max) {
            this.max = max;
            return this;
        }

        public IntegerVariable start(int start) {
            this.start = start;
            return this;
        }

        @Override
        public ModelDescription.IntegerVariable terminate() {
            return new ModelDescription.IntegerVariable() {
                @Override
                public String name() {
                    return name;
                }

                @Override
                public int valueReference() {
                    return index;
                }

                @Override
                public String description() {
                    return description;
                }

                @Override
                public String causality() {
                    return value(causality);
                }

                @Override
                public String variability() {
                    return value(variability);
                }

                @Override
                public String initial() {
                    return value(initial);
                }

                @Override
                public Integer previous() {
                    return index > 1 ? index - 1 : null;
                }

                @Override
                public boolean canHandleMultipleSetPerTimeInstant() {
                    return canHandleMultipleSetPerTime;
                }

                @Override
                public String quantity() {
                    return quantity;
                }

                @Override
                public Integer min() {
                    return min;
                }

                @Override
                public Integer max() {
                    return max;
                }

                @Override
                public Integer start() {
                    return start;
                }

            };
        }

    }

    public class BooleanVariable extends ScalarVariable<BooleanVariable> {
        private Boolean start;

        public BooleanVariable(String name, int valueReference) {
            super(name, valueReference);
        }

        public BooleanVariable start(boolean start) {
            this.start = start;
            return this;
        }

        @Override
        public ModelDescription.BooleanVariable terminate() {
            return new ModelDescription.BooleanVariable() {
                @Override
                public String name() {
                    return name;
                }

                @Override
                public int valueReference() {
                    return index;
                }

                @Override
                public String description() {
                    return description;
                }

                @Override
                public String causality() {
                    return value(causality);
                }

                @Override
                public String variability() {
                    return value(variability);
                }

                @Override
                public String initial() {
                    return value(initial);
                }

                @Override
                public Integer previous() {
                    return index > 1 ? index - 1 : null;
                }

                @Override
                public boolean canHandleMultipleSetPerTimeInstant() {
                    return canHandleMultipleSetPerTime;
                }

                @Override
                public Boolean start() {
                    return start;
                }

            };
        }

    }

    public class StringVariable extends ScalarVariable<StringVariable> {
        private String start;

        public StringVariable(String name, int valueReference) {
            super(name, valueReference);
        }

        public StringVariable start(String start) {
            this.start = start;
            return this;
        }

        @Override
        public ModelDescription.StringVariable terminate() {
            return new ModelDescription.StringVariable() {
                @Override
                public String name() {
                    return name;
                }

                @Override
                public int valueReference() {
                    return index;
                }

                @Override
                public String description() {
                    return description;
                }

                @Override
                public String causality() {
                    return value(causality);
                }

                @Override
                public String variability() {
                    return value(variability);
                }

                @Override
                public String initial() {
                    return value(initial);
                }

                @Override
                public Integer previous() {
                    return index > 1 ? index - 1 : null;
                }

                @Override
                public boolean canHandleMultipleSetPerTimeInstant() {
                    return canHandleMultipleSetPerTime;
                }

                @Override
                public String start() {
                    return start;
                }

            };
        }

    }

    public class ModelStructure {

        List<Unknown> outputs = new ArrayList<>();
        List<Unknown> derivatives = new ArrayList<>();
        List<Unknown> initialUnknowns = new ArrayList<>();

        public ModelStructure addOutput(Unknown unknown) {
            outputs.add(unknown);
            return this;
        }

        public ModelStructure addDerivative(Unknown unknown) {
            derivatives.add(unknown);
            return this;
        }

        public ModelStructure addInitialUnknowns(Unknown unknown) {
            initialUnknowns.add(unknown);
            return this;
        }

        public ModelDescription.ModelStructure terminate() {
            return new ModelDescription.ModelStructure() {
                @Override
                public ModelDescription.Outputs outputs() {
                    return new ModelDescription.Outputs() {
                        @Override
                        public List<ModelDescription.Unknown> unknowns() {
                            return outputs.stream().map(Unknown::terminate).collect(toList());
                        }
                    };
                }

                @Override
                public ModelDescription.Derivatives derivatives() {
                    return new ModelDescription.Derivatives() {
                        @Override
                        public List<ModelDescription.Unknown> unknowns() {
                            return derivatives.stream().map(Unknown::terminate).collect(toList());
                        }
                    };
                }

                @Override
                public ModelDescription.InitialUnknowns initialUnknowns() {
                    return new ModelDescription.InitialUnknowns() {
                        @Override
                        public List<ModelDescription.Unknown> unknowns() {
                            return initialUnknowns.stream().map(Unknown::terminate).collect(toList());
                        }
                    };
                }
            };
        }
    }

    public class Unknown {

        private String index;
        private List<String> dependencies = new ArrayList<>();
        private List<String> dependenciesKind = new ArrayList<>();

        public Unknown(String index) {
            this.index = index;
        }

        public Unknown dependencies(String... dependencies) {
            this.dependencies.addAll(asList(dependencies));
            return this;
        }

        public Unknown dependenciesKind(String... dependenciesKind) {
            this.dependenciesKind.addAll(asList(dependenciesKind));
            return this;
        }

        public ModelDescription.Unknown terminate() {
            return new ModelDescription.Unknown() {
                @Override
                public String index() {
                    return index;
                }

                @Override
                public String dependencies() {
                    return String.join(" ", dependencies);
                }

                @Override
                public String dependenciesKind() {
                    return String.join(" ", dependenciesKind);
                }
            };
        }
    }

}
