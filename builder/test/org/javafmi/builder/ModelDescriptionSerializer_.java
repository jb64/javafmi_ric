/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.builder;

import org.javafmi.framework.FmiContainer;
import org.javafmi.framework.ModelDescription;
import org.junit.Test;

import static org.javafmi.framework.FmiContainer.Causality.input;
import static org.javafmi.framework.FmiContainer.Initial.calculated;
import static org.javafmi.framework.FmiContainer.Variability.tunable;
import static org.javafmi.framework.ModelDescription.VariableNamingConvention.structured;

public class ModelDescriptionSerializer_ {

    public static FmiContainer setup() {
        return new FmiContainer();
    }

    @Test
    public void name() throws Exception {
        ModelDescription modelDescription = createModelDescription();
        String serialize = ModelDescriptionSerializer.serialize(modelDescription);
        System.out.println(serialize);
    }

    private ModelDescription createModelDescription() {
        return setup().model("modelName")
                .guid("guid12345")
                .description("description")
                .author("author")
                .modelVersion("model version 3.1")
                .copyright("copyright")
                .license("license")
                .variableNamingConvention(structured)
                .numberOfEventIndicators(101)

                .startTime(0.)
                .stopTime(1.)
                .tolerance(0.0001)
                .stepSize(0.02)

                .canHandleVariableCommunicationStepSize(true)
                .canInterpolateInputs(true)
                .maxOutputDerivativeOrder(3)
                .canRunAsynchronuously(true)
                .canBeInstantiatedOnlyOncePerProcess(false)
                .canNotUseMemoryManagementFunctions(false)
                .canGetAndSetFMUstate(true)
                .canSerializeFMUstate(true)
                .providesDirectionalDerivative(true)
                .add(createRealVariable())
                .add(createIntegerVariable())
                .add(createBooleanVariable())
                .add(createStringVariable())
                .add(createModelStructure())
                .terminate();
    }

    private FmiContainer.ModelStructure createModelStructure() {
        return setup().modelStructure()
                .addOutput(setup().unknown("1").dependencies("2", "6").dependenciesKind("fixed", "tunable"))
                .addOutput(setup().unknown("4").dependencies("3", "7").dependenciesKind("tunable", "fixed"))
                .addDerivative(setup().unknown("1").dependencies("2", "6").dependenciesKind("fixed", "tunable"))
                .addDerivative(setup().unknown("4").dependencies("3", "7").dependenciesKind("tunable", "fixed"))
                .addInitialUnknowns(setup().unknown("1").dependencies("2", "6").dependenciesKind("fixed", "tunable"))
                .addInitialUnknowns((setup().unknown("4").dependencies("3", "7").dependenciesKind("tunable", "fixed")));
    }

    private FmiContainer.RealVariable createRealVariable() {
        return setup().variable("real-variable").asReal()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .quantity("quantity")
                .unit("unit")
                .displayUnit("displayUnit")
                .relativeQuantity(false)
                .derivative(5)
                .min(0.)
                .max(1.)
                .nominal(.5)
                .start(.5)
                .isUnbounded(false)
                .reinit(false);
    }

    private FmiContainer.IntegerVariable createIntegerVariable() {
        return setup().variable("integer-variable").asInteger()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .quantity("quantity")
                .min(0)
                .max(10)
                .start(5);
    }

    private FmiContainer.BooleanVariable createBooleanVariable() {
        return setup().variable("boolean-variable").asBoolean()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .start(true);
    }


    private FmiContainer.StringVariable createStringVariable() {
        return setup().variable("string-variable").asString()
                .description("this is a test variable")
                .causality(input)
                .variability(tunable)
                .initial(calculated)
                .canHandleMultipleSetPerTime(false)
                .start("start value");
    }


}
