#include <fmi/fmi2FunctionTypes.h>
#include "catch.hpp"
#include "Poco/Thread.h"
#include "fakeit.hpp"
#include "../src/shelllibrary/LogSource.h"
#include "../src/shelllibrary/Logger.h"
#include "../src/shelllibrary/LogListener.h"

using namespace Poco;
using std::shared_ptr;
using std::make_shared;

using std::string;
using namespace fakeit;

shared_ptr<std::istream> twoMessages() {
	auto messages = make_shared<std::stringstream>();
	*messages << "message one" << std::endl << "message 2" << std::endl;
	return messages;
}


TEST_CASE("LogListenerTest") {
	auto mocked_source = Mock<LogSource>();
	auto mocked_logger = Mock<Logger>();
	LogSource& source = mocked_source.get();
	Logger& logger = mocked_logger.get();

	SECTION("reads from LogSource and writes in Logger") {
		auto message = "Mocked value";
		When(Method(mocked_source, getLine)).Return(message);
		When(Method(mocked_logger, log)).AlwaysReturn();
		When(Dtor(mocked_source)).AlwaysReturn();
		When(Dtor(mocked_logger)).AlwaysReturn();

		LogListener listener{ shared_ptr<LogSource>(&source), shared_ptr<Logger>(&logger) };
		listener.keepListening(false);
		listener.listen();
		Thread::sleep(100);
		Verify(Method(mocked_source, getLine));
		Verify(Method(mocked_logger, log).Using(message));
	}
}