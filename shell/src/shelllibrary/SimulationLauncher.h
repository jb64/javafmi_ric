#pragma once

#include <string>
#include "CommunicationHandle.h"
#include <fmi/fmi2Functions.h>

using std::string;

CommunicationHandle launchFmu(string basedir, string instance_name, const fmi2CallbackFunctions* callbacks);
bool doesFileExist(char* filename);
char* append(char* first, char* second, const fmi2CallbackFunctions* callbacks);