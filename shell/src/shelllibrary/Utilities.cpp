#include "Utilities.h"

string booleanToString(int boolean) {
	if (boolean == 0) return "false";
	return "true";
}

int stringToBoolean(const string& value) {
	if (value == "true") return 1;
	return 0;
}


int stringStartsWith(const string& body, const string& root) {
	return body.find(root) == 0;
}
