#pragma once
#include <string>
using std::string;

string booleanToString(int boolean);
int stringToBoolean(const string& value);
int stringStartsWith(const string& body, const string& root);
