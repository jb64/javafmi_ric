#include "Logger.h"
#include "LogParser.h"

Logger::Logger(fmi2CallbackLogger output) : output{ output } {}

Logger::~Logger() {}

void Logger::log(string message) {
	LogParser parser{ message };
	output(nullptr, parser.instanceName().c_str(), fmi2OK, parser.category().c_str(), parser.text().c_str());
}
