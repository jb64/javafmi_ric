#include "PermissionFixer.h"

void fixPermissions(const string& path){
#if defined(_WIN64)
#else
    string command = "find " + path + " -exec chmod 744 {} \\;";
    system(command.c_str());
#endif
}
