﻿#pragma once
#include <string>
#include <istream>
#include <memory>

using std::string;
using std::istream;
using std::shared_ptr;

class LogSource {
	static const int MAX_BUFFER_SIZE = 2048;
	shared_ptr<istream> input_stream;
	char buffer[MAX_BUFFER_SIZE];
public:
	explicit LogSource(shared_ptr<istream> input_stream);
	virtual ~LogSource();
	virtual string getLine();
};
