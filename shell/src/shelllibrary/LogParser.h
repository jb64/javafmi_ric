#include <string>

using std::string;

class LogParser {
	string input;
	string instance_name;
	string _category;
	string _text;
public:
	LogParser(string input);
	string instanceName();
	string category();
	string text();
};