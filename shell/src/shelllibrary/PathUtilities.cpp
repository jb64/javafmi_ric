#include "PathUtilities.h"
#include <Poco/File.h>
#include <functional>

using Poco::File;

#if defined(_WIN64)
#include "Utilities.h"

string fixPath(string& input_path) {
	if (input_path[0] == '/')
		input_path.erase(input_path.begin(), input_path.begin() + 1);
	if (input_path[input_path.size() - 1] == '/')
		input_path.erase(input_path.end() - 1, input_path.end());
	return input_path;
}
#else

string fixPath(string&& input_path) {
	if (input_path[input_path.size() - 1] == '/')
		input_path.erase(input_path.end() - 1, input_path.end());
	return "/" + input_path;
}

#endif


string extractResourcesPath(const string &resource_url, std::function<bool(string&)> checker) {
	auto scheme_index = resource_url.find_first_not_of("file://");
	auto resources_path = fixPath(resource_url.substr(scheme_index, resource_url.size()));
	if (checker(resources_path)) return resources_path;
	resources_path += "/resources";
	if (checker(resources_path)) return resources_path;
	throw InvalidResourcesDirectory(resource_url);
}
