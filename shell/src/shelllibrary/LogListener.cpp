#include "LogListener.h"

LogListener::LogListener(shared_ptr<LogSource> log_source, shared_ptr<Logger> logger) :
log_source(log_source), logger(logger) {}

void LogListener::listen() {
	listen_thread.start(*this);
}

void LogListener::stop() {
	keepListening(false);
	listen_thread.join();
}

void LogListener::keepListening(bool value) {
	continue_listening = value;
}


void LogListener::run() {
	do {
		auto message = log_source->getLine();
		if (!message.empty())
			logger->log(message);
	} while (continue_listening);
}