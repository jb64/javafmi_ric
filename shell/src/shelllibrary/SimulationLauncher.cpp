#include "SimulationLauncher.h"
#include <fmi/fmi2Functions.h>
#include <sys/stat.h>
#include <cstring>

#if defined(_WIN64)
#define JRE_BIN "bin/java.exe"
Process::Args& addClasspath(Process::Args&& args, const string& basedir) {
	args.push_back("-classpath \"" + basedir + "/*\"");
	return args;

}

string fixInstanceName(string instance_name) {
	return "\"" + instance_name + "\"";
}
#elif defined(__linux)
#define JRE_BIN "bin/java"
Process::Args& addClasspath(Process::Args&& args, const string& basedir) {
	args.push_back("-classpath");
	args.push_back(basedir + "/*");
	return args;
}

string fixInstanceName(string instance_name) {
	return instance_name;
}
#endif



using namespace Poco;

using std::string;

CommunicationHandle launchFmu(string basedir, string instance_name, const fmi2CallbackFunctions* callbacks) {
	Pipe in_pipe;
	Pipe out_pipe;
	Pipe error_pipe;
	setlocale(LC_NUMERIC, "C");
	char* environment{ getenv("JAVA_HOME") };
	if (environment == NULL) {
		callbacks->logger(nullptr, "launching simulation", fmi2Fatal, "ERROR", "JAVA_HOME environment variable is not set");
		exit(1);
	}

	char *command{ append(environment, JRE_BIN, callbacks) };

	if (!doesFileExist(command)) {
		callbacks->logger(nullptr, "launching simulation", fmi2Fatal, "ERROR", "JRE not found. Set JAVA_HOME correctly or reboot application");
		exit(1);
	}

	string main_class{ "org/javafmi/skeleton/SimulationSkeleton" };
	auto args = addClasspath(Process::Args{}, basedir);
	args.push_back(main_class);
	args.push_back(fixInstanceName(instance_name));
	auto process_handle = std::make_shared<ProcessHandle>(
		Process::launch(command, args, basedir, &in_pipe, &out_pipe, &error_pipe));
	auto process_out = std::make_shared<PipeInputStream>(out_pipe);
	auto process_error = std::make_shared<PipeInputStream>(error_pipe);
	auto process_in = std::make_shared<PipeOutputStream>(in_pipe);
	callbacks->freeMemory(command);
	return CommunicationHandle{ process_handle, process_in, process_out, process_error };
};

bool doesFileExist(char* filename) {
	struct stat st;
	int result = stat(filename, &st);
	return result == 0;
}

char* append(char* first, char* second, const fmi2CallbackFunctions* callbacks) {
	char* result = (char*) callbacks->allocateMemory(strlen(first) + strlen(second) + 1, 1);
	strcpy(result, first);
	strcat(result, second);
	return result;
}
