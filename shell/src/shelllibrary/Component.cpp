#include "Component.h"

Component::Component(CommunicationHandle channel, shared_ptr<const fmi2CallbackFunctions> callbacks)
	: channel{ channel },
	callbacks{ callbacks } {
	auto log_source = std::make_shared<LogSource>(channel.outChannel());
	auto logger = std::make_shared<Logger>(callbacks->logger);
	log_listener = std::make_shared<LogListener>(log_source, logger);
}