﻿#include "ErrorFileHandler.h"
#include <Poco/Glob.h>
#include <Poco/File.h>
#include <Poco/Path.h>
#include <algorithm>

using namespace Poco;

ErrorFileHandler::ErrorFileHandler(string error_directory) {
	this->error_directory = Path(error_directory).toString(Path::PATH_NATIVE);
}

ErrorFileHandler::~ErrorFileHandler() {}

void ErrorFileHandler::copyErrorFileTo(const string& destination_directory) {
	std::set<string> generated_error_files;
	Glob::glob(error_directory + "/*_error.log", generated_error_files);
	for_each(generated_error_files.begin(), generated_error_files.end(),
	         [&](const string& error_file) {
		         File{error_file}.copyTo(destination_directory);
	         });
}
