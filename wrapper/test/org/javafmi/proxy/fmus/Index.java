/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy.fmus;

import org.javafmi.modeldescription.v1.ModelDescription;
import org.javafmi.proxy.FmuFile;
import org.javafmi.proxy.v1.FmuProxy;
import org.javafmi.proxy.v2.Fmu;

import java.io.File;

import static org.javafmi.kernel.OS.isLinux32;
import static org.javafmi.kernel.OS.isWin32;

public class Index {

    private static final String fmuPath = "./test-res/fmu/cosimulation/";

    public static final String Win32_v1 = "win_32_SFR_CoSimulation_V1.fmu";
    public static final String Win32_v2_RC1 = "win_32_64_HTB_0REUNION_V2RC1.fmu";
    public static final String Win32_v2 = "win_32_bouncingBall.fmu";
    public static final String Linux32 = "linux_32_plusun.fmu";
    public static final String Linux64_v2 = "linux_64_V2_tankv3.fmu";
    public static final String WithDirectionalDerivatives = "PackageBuildingCases_ComponentsFMU_wallFmu1.fmu";
    public static final String WithIncrements = "win_32_inc.fmu";
    public static final String WithReadableBoolean = "win_32_values.fmu";
    public static final String WithReadableInteger = "win_32_inc.fmu";
    public static final String WithWritableInteger = "win_32_values.fmu";
    public static final String WithReadableString = "win_32_values.fmu";
    public static final String WithReadableReal = "win_32_bouncingBall.fmu";
    public static final String WithWritableReal = "win_32_bouncingBall.fmu";
    public static final String WithReadableEnumeration = "win_32_GridSysPro_0V1_Examples_Validation_0with_0generator.fmu";
    public static final String WithResources = "fmu_with_resources.fmu";
    public static final String WithResourcesButEmpty = "fmu_with_resources_but_empty.fmu";

    public static FmuFile createFmuFile(String fmuName) {
        return new FmuFile(new File(getFmuPath(fmuName)));
    }

    public static String getFmuPath(String fmuName) {
        return fmuPath + fmuName;
    }

    public static FmuProxy createProxy(FmuFile fmuFile) {
        return new org.javafmi.proxy.v1.FmuProxy(fmuFile.getLibraryPath(), (ModelDescription) fmuFile.getModelDescription());
    }

    public static org.javafmi.proxy.v2.FmuProxy createV2FmuProxy(FmuFile fmuFile) {
        return new org.javafmi.proxy.v2.FmuProxy(new Fmu(fmuFile.getLibraryPath()), (org.javafmi.modeldescription.v2.ModelDescription) fmuFile.getModelDescription());
    }

    public static String getFmuNameForThisOS() {
        if (isWin32()) return Win32_v1;
        if (isLinux32()) return Linux32;
        throw new RuntimeException("There is not an FMU for this system");
    }

    public static String getV2FmuNameForThisOS() {
        if (isWin32()) return Win32_v2_RC1;
        return null;
    }
}
