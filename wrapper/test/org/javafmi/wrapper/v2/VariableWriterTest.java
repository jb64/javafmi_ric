/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v2;

import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.modeldescription.v2.*;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.wrapper.variables.VariableWriter;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class VariableWriterTest {

    private FmiProxy mockProxy;
    private VariableWriter writer;
    private ModelDescription mockedModelDescription;

    @Before
    public void setUp() {
        mockProxy = mock(org.javafmi.proxy.v2.FmuProxy.class);
        mockedModelDescription = mock(ModelDescription.class);
        writer = new VariableWriter(mockProxy, mockedModelDescription);
    }

    @Test
    public void writeARealVariableCallsToSetRealInProxy() {
        String realVariableName = "";
        double[] realVariableValue = {0.0};
        int[] realVariableValueReference = {0};
        configureTest(new RealVariable(realVariableName).withValueReference(realVariableValueReference[0]), realVariableValue[0]);
        verify(mockProxy).setReal(realVariableValueReference, realVariableValue);
    }

    @Test
    public void writeAIntegerVariableCallsToSetIntegerInProxy() {
        String integerVariableName = "";
        Integer integerVariableValue = 0;
        Integer integerVariableValueReference = 0;
        configureTest(new IntegerVariable(integerVariableName).withValueReference(integerVariableValueReference), integerVariableValue);
        verify(mockProxy).setInteger(new int[]{integerVariableValueReference}, new int[]{integerVariableValue});
    }

    @Test
    public void writeABooleanVariableCallsToSetBooleanInProxy() {
        String booleanVariableName = "";
        boolean booleanVariableValue = true;
        int booleanVariableValueReference = 0;
        configureTest(new BooleanVariable(booleanVariableName).withValueReference(booleanVariableValueReference), booleanVariableValue);
        verify(mockProxy).setBoolean(new int[]{booleanVariableValueReference}, new boolean[]{booleanVariableValue});
    }

    @Test
    public void writeAStringVariableCallsToSetStringInProxy() {
        String stringVariableName = "";
        String stringVariableValue = "";
        int stringVariableValueReference = 0;
        configureTest(new StringVariable(stringVariableName).withValueReference(stringVariableValueReference), stringVariableValue);
        verify(mockProxy).setString(new int[]{stringVariableValueReference}, new String[]{stringVariableValue});
    }

    private void configureTest(ScalarVariable variable, Object value) {
        when(mockedModelDescription.getModelVariable(variable.getName())).thenReturn(variable);
        writer.write(variable.getName(), value);
    }
}
