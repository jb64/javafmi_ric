/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v2;

import org.javafmi.proxy.v2.State;
import org.javafmi.wrapper.Simulation;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.not;
import static org.javafmi.kernel.OS.isWin32;
import static org.javafmi.kernel.OS.isWin64;
import static org.javafmi.wrapper.CommonsWrapper.INDOOR_HEAT_GAINS;
import static org.javafmi.wrapper.CommonsWrapper.createSimulation;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeTrue;


public class StateManipulationTest {
    @Test
    public void serializedStateIsValid() {
        assumeTrue(isWin64() || isWin32());
        Simulation simulation = createSimulation(INDOOR_HEAT_GAINS);
        simulation.init(0., 10.);
        Access access = new Access(simulation);
        access.doStep(0., 0.1);
        State stateAfterOneStep = access.getState(State.newEmptyState());
        access.doStep(0.1, 0.1);
        State stateAfterTwoSteps = access.getState(State.newEmptyState());
        access.setState(stateAfterOneStep);

        assertThat(stateAfterOneStep.GetPointer(), is(not(stateAfterTwoSteps.GetPointer())));
        assertThat(stateAfterOneStep.GetPointer(), is(notNullValue()));
        assertThat(stateAfterTwoSteps.GetPointer(), is(notNullValue()));

        access.freeState(stateAfterOneStep);
        access.freeState(stateAfterTwoSteps);
        simulation.terminate();
    }

}
