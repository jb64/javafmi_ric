/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v2;

import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.modeldescription.v2.BooleanVariable;
import org.javafmi.modeldescription.v2.IntegerVariable;
import org.javafmi.modeldescription.v2.RealVariable;
import org.javafmi.modeldescription.v2.StringVariable;
import org.javafmi.proxy.FmiProxy;
import org.javafmi.wrapper.variables.VariableReader;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class VariableReaderTest {

    private VariableReader reader;
    private FmiProxy mockProxy;
    private ModelDescription mockedModelDescription;

    @Before
    public void setUp() {
        mockProxy = mock(org.javafmi.proxy.v2.FmuProxy.class);
        mockedModelDescription = mock(ModelDescription.class);
        reader = new VariableReader(mockProxy, mockedModelDescription);
    }

    @Test
    public void readingARealCallsToGetRealInProxy() {
        String realVariableName = "";
        int[] realVariableValueReference = new int[]{0};
        configureRealTest(new RealVariable(realVariableName).withValueReference(realVariableValueReference[0]));
        verify(mockProxy).getReal(realVariableValueReference);
    }

    @Test
    public void readingAnIntegerCallsToGetIntegerInProxy() {
        String intVariableName = "";
        Integer intVariableValueReference = 0;
        configureIntTest(new IntegerVariable(intVariableName).withValueReference(intVariableValueReference));
        verify(mockProxy).getInteger(intVariableValueReference);
    }

    @Test
    public void readingABooleanCallsToGetBooleanInProxy() {
        String booleanVariableName = "";
        Integer booleanVariableValueReference = 0;
        configureBooleanTest(new BooleanVariable(booleanVariableName).withValueReference(booleanVariableValueReference));
        verify(mockProxy).getBoolean(booleanVariableValueReference);
    }

    @Test
    public void readingAStringCallsToGetStringInProxy() {
        String stringVariableName = "";
        Integer stringVariableValueReference = 0;
        configureStringTest(new StringVariable(stringVariableName).withValueReference(stringVariableValueReference));
        verify(mockProxy).getString(stringVariableValueReference);
    }

    private void configureRealTest(org.javafmi.modeldescription.v2.ScalarVariable variable) {
        when(mockProxy.getReal(anyInt())).thenReturn(new double[]{anyDouble()});
        configure(variable);
    }

    private void configureIntTest(org.javafmi.modeldescription.v2.ScalarVariable variable) {
        when(mockProxy.getInteger(anyInt())).thenReturn(new int[]{anyInt()});
        configure(variable);
    }

    private void configureBooleanTest(org.javafmi.modeldescription.v2.ScalarVariable variable) {
        when(mockProxy.getBoolean(anyInt())).thenReturn(new boolean[]{anyBoolean()});
        configure(variable);
    }

    private void configureStringTest(org.javafmi.modeldescription.v2.ScalarVariable variable) {
        when(mockProxy.getString(anyInt())).thenReturn(new String[]{anyString()});
        configure(variable);
    }

    private void configure(org.javafmi.modeldescription.v2.ScalarVariable variable) {
        when(mockedModelDescription.getModelVariable(variable.getName())).thenReturn(variable);
        mockProxy.initialize(0, 0);
        reader.read(variable.getName());
    }

}
