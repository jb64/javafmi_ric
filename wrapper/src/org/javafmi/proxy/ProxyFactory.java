/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.proxy;


import org.javafmi.modeldescription.FmiVersion;
import org.javafmi.modeldescription.ModelDescription;
import org.javafmi.proxy.v2.Fmu;
import org.javafmi.proxy.v2.JavaFmuProxy;

import static org.javafmi.kernel.Convention.FrameworkManufacturer;
import static org.javafmi.kernel.Convention.FrameworkVersion;

public class ProxyFactory {

	public static FmiProxy createProxy(ModelDescription modelDescription, String libraryPath) {
		if (modelDescription.getFmiVersion().equals(FmiVersion.One))
			return new org.javafmi.proxy.v1.FmuProxy(libraryPath, (org.javafmi.modeldescription.v1.ModelDescription) modelDescription);
		else if (isJavaFMU(modelDescription) && isSameVersion(modelDescription))
			return new JavaFmuProxy((org.javafmi.modeldescription.v2.ModelDescription) modelDescription);
		else return new org.javafmi.proxy.v2.FmuProxy(new Fmu(libraryPath),
				(org.javafmi.modeldescription.v2.ModelDescription) modelDescription);
	}

	private static boolean isJavaFMU(ModelDescription modelDescription) {
		return modelDescription.getGenerationTool().startsWith(FrameworkManufacturer);
	}

	private static boolean isSameVersion(ModelDescription modelDescription) {
		return warn(modelDescription.getGenerationTool().endsWith(FrameworkVersion));
	}

	private static boolean warn(boolean sameVersion) {
		if (!sameVersion) System.out.println("WARNING: This wrapper version is not compatible with this JavaFMU. Performance will be worse");
		return sameVersion;
	}
}
